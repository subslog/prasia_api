
# 설치 모듈
import requests
# 프로젝트 모듈
from src.utils.date import current_time
from src.utils.my_logging import set_logger
# 예외 처리 에러
from src.error_make import NotTextError

# 로거 객체 생성
logger = set_logger('/var/log/prasia_work/', 'prasia_work.log', 'prasia_work')

def send(channel_url: str, text: str):
    """channel_url의 채널로 text 메시지를 전송"""

    # 디스코드 메시지 전송 시작 로깅
    logger.info('----------Start sending Discord messages----------')
    # 디스코드 채널 url, 전송 메시지 로깅
    logger.info(f'url : {channel_url}')
    logger.info(f'message : {text}')

    try:
        # 텍스트가 없으면 요청을 하지 않는다.
        if not text: raise NotTextError('The message to send to the Discord channel is empty.')
    # 텍스트가 비어있을 때 warning 로깅
    except NotTextError as e:
        logger.warning(f'An warning occurred: {e}', exc_info=True)
        return 0
    # 이외의 에러 warning 로깅
    except Exception as e:
        logger.warning(f'An warning occurred: {e}', exc_info=True)
        return 0

    # 전송할 메시지
    message = {'content': f'{text}'}
    # 메시지 전송
    requests.post(channel_url, data=message)

    # 디스코드 메시지 전송 종료 로깅
    logger.info(f'----------End sending Discord messages----------')