class NotTextError(Exception):
   def __init__(self, messages='The text is empty.'):
        super().__init__(messages)

class RankingCallFailed(Exception):
   def __init__(self, messages='Ranking information could not be loaded.'):
        super().__init__(messages)

class GcidNotFound(Exception):
   def __init__(self, gcid, messages='not found in gc_infos.'):
        super().__init__(f'gcid({gcid}) {messages}')