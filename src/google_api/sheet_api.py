# 표준 라이브러리
import sys
# 설치 모듈
from google.oauth2 import service_account
from googleapiclient.discovery import build
# 프로젝트 모듈
from src.utils.my_logging import set_logger
# 예외 처리 에러
from googleapiclient.errors import UnknownApiNameOrVersion
from google.auth.exceptions import DefaultCredentialsError
from googleapiclient.errors import HttpError

# 로거 객체 생성
logger = set_logger('/var/log/prasia_work/', 'prasia_work.log', 'prasia_work')

def sheet_api_settings(api_key_path: str, scopes: list):
    """Sheets API 호출에 필요한 셋팅을 하는 함수"""
    
    # Sheet API 셋팅 시작 로깅
    logger.info('----------Start setting up Sheet API----------')
    # API 인증 키 json 파일, scopes 로깅
    logger.info(f'API Key Path : {api_key_path}')
    logger.info(f'Scopes : {scopes}')
    
    try:
        # 자격 증명 설정
        creds = service_account.Credentials.from_service_account_file(api_key_path, scopes=scopes)
        # 서비스 빌드
        service = build('sheets', 'v4', credentials=creds)
    # API 인증 키 json 파일을 찾지 못하면 에러
    except FileNotFoundError as e:
        logger.error(f'An error occurred: {e}', exc_info=True)
        sys.exit(1)
    # API 서비스 또는 버전 잘못 입력되면 에러
    except UnknownApiNameOrVersion as e:
        logger.error(f'An error occurred: {e}', exc_info=True)
        sys.exit(1)
    # 자격 증명 관련 에러
    except AttributeError as e:
        logger.error(f'An error occurred: {e}', exc_info=True)
        sys.exit(1)
    # 자격 증명 관련 에러
    except DefaultCredentialsError as e:
        logger.error(f'An error occurred: {e}', exc_info=True)
        sys.exit(1)
    
    # Sheet API 셋팅 완료 로깅
    logger.info('----------Sheet API setup complete----------')
    
    return service

def update_sheet(service, spreadsheet_id: str, sheet_name: str, range: str, data: list) -> None:
    """단일 범위로 data를 업데이트하는 함수"""

    # Sheet 업데이트 시작 로깅
    logger.info('----------Start sheet update----------')
    
    # Sheets API 입력 형태로 범위, 데이터 변경
    range = f'{sheet_name}!{range}'
    body = {'values': data}
    
    # 입력 범위 및 데이터 1행 로깅
    logger.info(f'Update range : {range}')
    logger.info(f'Values : {body['values'][0]}')
    
    try:
        result = (
            service.spreadsheets()
            .values()
            .update(spreadsheetId=spreadsheet_id, range=range, valueInputOption='RAW',body=body)
            .execute()
        )
    except AttributeError as e:
        logger.error(f'An error occurred: {e}', exc_info=True)
        sys.exit(1)
    except HttpError as e:
        logger.error(f'An error occurred: {e}', exc_info=True)
        sys.exit(1)
    
    # Sheet 업데이트 완료 로깅
    logger.info('----------Sheet update completed----------')
    
def delete_values(service, spreadsheet_id: str, sheet_name: str, range: str):
    """range 범위의 데이터를 지우는 함수"""
    
    # Sheet 데이터 삭제 시작 로깅
    logger.info('----------Start delete sheet data----------')
    
    # Sheets API 입력 형태로 범위, 데이터 변경
    range = f'{sheet_name}!{range}'
    
    # 삭제 범위 로깅
    logger.info(f'Delete range : {range}')
    
    try:
        result = (
            service.spreadsheets()
            .values()
            .clear(spreadsheetId=spreadsheet_id, range=range)
            .execute()
        )
    except AttributeError as e:
        logger.error(f'An error occurred: {e}', exc_info=True)
        sys.exit(1)
    except HttpError as e:
        logger.error(f'An error occurred: {e}', exc_info=True)
        sys.exit(1)
    
    # Sheet 데이터 삭제 시작 로깅
    logger.info('----------End delete sheet data----------')

def row_cnt_sheet(service, spreadsheet_id: str, sheet_name: str) -> int:
    """시트의 행 수를 반환하는 함수"""
    
    try:
        result = (
            service.spreadsheets()
            .get(spreadsheetId=spreadsheet_id, ranges=sheet_name, fields='sheets.properties')
            .execute()
        )
    except AttributeError as e:
        logger.error(f'An error occurred: {e}', exc_info=True)
        sys.exit(1)
    except HttpError as e:
        logger.error(f'An error occurred: {e}', exc_info=True)
        sys.exit(1)
    
    row_cnt: int = result['sheets'][0]['properties']['gridProperties']['rowCount']
    
    # Row 수 로깅
    logger.info(f'{sheet_name} sheet row count : {row_cnt} Row')
    
    return row_cnt

def update_filter(service, spreadsheet_id: str, filterviews: list) -> None:
    """
    필터 범위 재설정 함수
    filter_list 함수의 반환되는 구조 사용
    filterviews: [
        {
            'filterViewId': 138317517,
            'title': '필터 이름',
            'range': {
                'sheetId': 스프레드시트 id
                'startRowIndex': 0,
                'endRowIndex': 8401,
                'startColumnIndex': 0,
                'endColumnIndex': 8
            }
        },
        ...
    ]
    """
    
    # 필터 범위 업데이트 시작 로깅
    logger.info('----------Start filter range update----------')
    
    # reuqets 생성
    requests = []
    for filter in filterviews:
        requests.append(
            {
                'updateFilterView': {
                    'filter': filter,
                    'fields': 'range'
                }
            }
        )
    
    # 요청 body 생성
    body = {
        'requests': requests
    }
    
    try:
        # API 요청
        result = (
        service.spreadsheets()
        .batchUpdate(spreadsheetId=spreadsheet_id, body=body)
        .execute()
        )
    except AttributeError as e:
        logger.error(f'An error occurred: {e}', exc_info=True)
        sys.exit(1)
    except HttpError as e:
        logger.error(f'An error occurred: {e}', exc_info=True)
        sys.exit(1)
    
    # 필터 범위 업데이트 시작 로깅
    logger.info('----------End filter range update----------')

def filter_list(service, spreadsheet_id: str, sheet_name: str) -> list:
    """시트의 필터 리스트를 반환하는 함수"""
    
    try:
    # API 요청
        result = (
            service.spreadsheets()
            .get(spreadsheetId=spreadsheet_id, ranges=sheet_name, fields='sheets/filterViews')
            .execute()
        )
    except AttributeError as e:
        logger.error(f'An error occurred: {e}', exc_info=True)
        sys.exit(1)
    except HttpError as e:
        logger.error(f'An error occurred: {e}', exc_info=True)
        sys.exit(1)
    
    # 필터 리스트
    filterviews: list = result['sheets'][0]['filterViews']
    
    # 필터 리스트 로깅
    logger.info(f'{sheet_name} sheet filter list : {filterviews}')
    
    return filterviews

def get_values(service, spreadsheet_id: str, sheet_name: str, range: str):
    """스프레드시트의 값을 불러오는 함수"""

    # Sheets API 입력 형태로 범위, 데이터 변경
    range = f'{sheet_name}!{range}'
    
    # 시트 검색 범위 로깅
    logger.info(f'Sheet value retrieval range : {range}')
    
    try:
        # 데이터 get
        result = (
            service.spreadsheets()
            .values()
            .get(spreadsheetId=spreadsheet_id, range=range)
            .execute()
        )
    except AttributeError as e:
        logger.error(f'An error occurred: {e}', exc_info=True)
        sys.exit(1)
    except HttpError as e:
        logger.error(f'An error occurred: {e}', exc_info=True)
        sys.exit(1)
    
    # 수집 데이터 로깅
    logger.debug(result)

    return result

if __name__ == '__main__':
    import sys
    import os
    
    # 현재 스크립트의 상위 디렉터리 경로
    parent_dir = os.path.dirname(os.path.dirname(__file__))
    
    # 구글 API 인증키 경로
    json_file_path: str = f'{parent_dir}/resources/prasia-ranking-sheet-8a83c232523c.json'
    # 구글 스프레드 시트 읽기, 쓰기 권한
    scopes: list = ['https://www.googleapis.com/auth/spreadsheets']
    # API 서비스 셋팅
    service = sheet_api_settings(json_file_path, scopes)
    # 스프레드시트 id
    spreadsheet_id='1FG_Wgvz9b_dfUOWQffL4RR0nK7j56TzaYvoz-yOWIlM'
    # 작업 시트 이름
    work_sheet_name = 'test'
    
    # 캐릭터 정보 저장할 리스트
    gc_info = [
        [1, '1111500000080000032', '아우리엘01', '샤코랑', '강남', '16', 77],
        [2, '1111500000000000164', '아우리엘01', '전우치랑', '강남', '16', 77],
        [3, '1111500000000001247', '아우리엘01', '물찬곰돌이', '강남', '16', 76],
        [4, '1116100000240002402', '아우리엘01', '하입보리', '강남', '15', 75],
        [5, '1103200000000000148', '아우리엘01', '엘프랑', '강남', '15', 75],
        [6, '1111500000000000111', '아우리엘01', '이수랑', '강북', '15', 75],
        [7, '1111500000000000456', '아우리엘01', '웃어랑', '강남', '14', 75],
        [8, '1103200000380000130', '아우리엘01', '사자랑', '강남', '15', 75], 
        [9, '1111100000000003132', '아우리엘01', '약사랑', '강북', '14', 75],
        [10, '1111500000000000128', '아우리엘01', '동우랑', '강남', '15', 74]
    ]
    
    # A2 ~ G11까지 데이터를 업데이트
    if sys.argv[1] == 'update_sheet':
        update_sheet(service, spreadsheet_id, work_sheet_name, 'A2', gc_info)
    # A2 ~ G3까지 데이터를 삭제
    elif sys.argv[1] == 'delete_values':
        delete_values(service, spreadsheet_id, work_sheet_name, 'A2:G3')
    # 시트의 행 수를 출력
    elif sys.argv[1] == 'row_cnt_sheet':
        print(row_cnt_sheet(service, spreadsheet_id, work_sheet_name))
    # 시트의 모든 필터 범위를 1 ~ 101행까지 업데이트
    elif sys.argv[1] == 'update_filter':
        # 필터 리스트
        filterviews = filter_list(service, spreadsheet_id, work_sheet_name)
        
        # 필터 범위 설정
        for filfer in filterviews:
            # 필터 시작 행
            filfer['range']['startRowIndex'] = 0
            # 필터 끝 행
            filfer['range']['endRowIndex'] = 101
            # 필터 시작 열
            filfer['range']['startColumnIndex'] = 0
            # 필터 시작 열
            filfer['range']['endColumnIndex'] = 7
        
        # 필터 업데이트
        update_filter(service, spreadsheet_id, filterviews)
    else:
        print('존재하지 않는 옵션')