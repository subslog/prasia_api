# 표준 라이브러리
import sys
# 설치 모듈
from google.oauth2 import service_account
from googleapiclient.discovery import build
from googleapiclient.http import MediaFileUpload
# 프로젝트 모듈
from src.utils.my_logging import set_logger
# 예외 처리 에러
from googleapiclient.errors import UnknownApiNameOrVersion
from googleapiclient.errors import ResumableUploadError
from googleapiclient.errors import HttpError
from google.auth.exceptions import DefaultCredentialsError

# 로거 객체 생성
logger = set_logger('/var/log/prasia_work/', 'prasia_work.log', 'prasia_work')

def drive_api_settings(api_key_path: str, scopes: list):
    """Drive API 호출에 필요한 셋팅을 하는 함수"""

    # Drive API 셋팅 시작 로깅
    logger.info('----------Start setting up Drive API----------')
    # API 인증 키 json 파일, scopes 로깅
    logger.info(f'API Key Path : {api_key_path}')
    logger.info(f'Scopes : {scopes}')

    try:
        # 자격 증명 설정
        creds = service_account.Credentials.from_service_account_file(api_key_path, scopes=scopes)
        # 서비스 빌드
        service = build('drive', 'v3', credentials=creds)
    # API 인증 키 json 파일을 찾지 못하면 에러
    except FileNotFoundError as e:
        logger.error(f'An error occurred: {e}', exc_info=True)
        sys.exit(1)
    # API 서비스 또는 버전 잘못 입력되면 에러
    except UnknownApiNameOrVersion as e:
        logger.error(f'An error occurred: {e}', exc_info=True)
        sys.exit(1)
    # 자격 증명 관련 에러
    except AttributeError as e:
        logger.error(f'An error occurred: {e}', exc_info=True)
        sys.exit(1)
    # 자격 증명 관련 에러
    except DefaultCredentialsError as e:
        logger.error(f'An error occurred: {e}', exc_info=True)
        sys.exit(1)
    
    # Drive API 셋팅 완료 로깅
    logger.info('----------Drive API setup complete----------')

    return service

def upload_drive(service, drive_id: str, mimetype: str, file_path: str, save_name: str) -> None:
    """{file_path} 파일을 {save_name}으로 {drive_id} 드라이브에 업로드하는 함수"""

    # Drive 업로드 시작 로깅
    logger.info('----------Start drive upload----------')
    # service, drive_id, mimetype, file_path, save_name 로깅
    logger.info(f'Service : {service}')
    logger.info(f'Drive_id : {drive_id}')
    logger.info(f'Mimetype : {mimetype}')
    logger.info(f'File_path : {file_path}')
    logger.info(f'Save_name : {save_name}')
    
    # 업로드 파일 메타데이터
    file_metadata = {'name': save_name, 'parents': [drive_id]}
    
    try:
        # 업로드 파일 정보
        media = MediaFileUpload(
            file_path, mimetype=mimetype, resumable=True
        )
        # 구글 드라이드에 파일 업로드
        file = (
            service.files()
            .create(body=file_metadata, media_body=media, fields='id')
            .execute()
        )
    # 업로드 파일을 찾지 못하면 에러
    except FileNotFoundError as e:
        logger.error(f'An error occurred: {e}', exc_info=True)
        sys.exit(1)
    # service 객체가 아닐 경우 에러
    except AttributeError as e:
        logger.error(f'An error occurred: {e}', exc_info=True)
        sys.exit(1)
    # Drive 업로드 에러
    except ResumableUploadError as e:
        logger.error(f'An error occurred: {e}', exc_info=True)
        sys.exit(1)
    
    # 업로드 파일 정보
    logger.info(f'Upload file info : {file}')
    # Drive 업로드 완료 로깅
    logger.info('----------Drive upload complete----------')

def load_drive(service, query: str = '', pageSize: int=None, files: list=['id', 'name'], orderBy: str = None) -> None:
    """구글 드라이브에 권한이 있는 파일의 정보를 반환하는 함수"""

    # Drive 불러오기 시작 로깅
    logger.info('----------Start loading drive----------')
    # service, query, pageSize, files, orderBy 로깅
    logger.info(f'Service : {service}')
    logger.info(f'Query : {query}')
    logger.info(f'PageSize : {pageSize}')
    logger.info(f'Column : {files}')
    logger.info(f'OrderBy : {orderBy}')
    
    try:
        # 구글 드라이드 리스트 요청
        results = (
        service.files()
        .list(
            q=query,
            pageSize=pageSize,
            fields=f'nextPageToken, files({','.join(files)})',
            orderBy=orderBy
        )
        .execute()
        )
    # service 객체가 아닐 경우 에러
    except AttributeError as e:
        logger.error(f'An error occurred: {e}', exc_info=True)
        sys.exit(1)
    # HTTP 요청 에러
    except HttpError as e:
        logger.error(f'An error occurred: {e}', exc_info=True)
        sys.exit(1)
    # 잘못된 타입을 매개변수로 넣었을 때 에러
    except TypeError as e:
        logger.error(f'An error occurred: {e}', exc_info=True)
        sys.exit(1) 

    # Drive에서 불러온 파일들
    logger.info(f'Drive loading files : {results['files']}')
    # Drive 불러오기 완료 로깅
    logger.info('----------Drive loading complete----------')
    
    return results

def read_text_file(service, file_id: str) -> None:
    """구글 드라이브에 권한이 있는 파일의 정보를 반환하는 함수"""

    # Drive 텍스트 파일 읽기 시작
    logger.info('----------Start reading Drive text files----------')
    # service, fild_id 로깅
    logger.info(f'Service : {service}')
    logger.info(f'File_id : {file_id}')
    
    try:
        # file_id 파일의 텍스트를 반환
        results = (
            service.files()
            .get_media(
                fileId=file_id
            )
            .execute()
        )
        # 엔코딩 타입 변경
        text = results.decode('utf-8')
    # service 객체가 아닐 경우 에러
    except AttributeError as e:
        logger.error(f'An error occurred: {e}', exc_info=True)
        sys.exit(1)
    # HTTP 요청 에러
    except HttpError as e:
        logger.error(f'An error occurred: {e}', exc_info=True)
        sys.exit(1)
    # 잘못된 타입을 매개변수로 넣었을 때 에러
    except TypeError as e:
        logger.error(f'An error occurred: {e}', exc_info=True)
        sys.exit(1)
        
    # Drive 텍스트 파일 읽기 완료
    logger.debug(f'text file content : {text}')
    # Drive 텍스트 파일 읽기 완료
    logger.info('----------Drive text file read complete----------')
    
    return text