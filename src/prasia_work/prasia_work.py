# 표준 라이브러리
import os
# 프로젝트 모듈
from src.google_api.sheet_api import sheet_api_settings
from src.google_api.sheet_api import update_sheet
from src.google_api.sheet_api import filter_list
from src.google_api.sheet_api import update_filter
from src.google_api.sheet_api import row_cnt_sheet
from src.google_api.sheet_api import delete_values
from src.google_api.sheet_api import update_sheet
from src.google_api.sheet_api import get_values
from src.google_api.drive_api import drive_api_settings
from src.google_api.drive_api import upload_drive
from src.utils.json_work import load
from src.utils.json_work import dump
from src.utils.date import current_time
from src.utils.date import convert_time_to_timestamp
from src.utils.date import convert_timestamp_to_time
from src.utils.os_work import get_file_list
from src.utils.my_logging import set_logger
from src.utils.db_connector import connect_to_db
from src.utils.db_connector import execute_query
from src.discord.send_message import send
from src.prasia_work.api_call import ranking_call
from src.prasia_work.server_info import server_export
# 예외 처리 에러
from src.error_make import GcidNotFound

class Prasia_work:
    def __init__(self, service_mode: str):
        # 현재 스크립트 절대경로
        self.script_dir = os.path.dirname(os.path.abspath(__file__))
        
        # 구글 API 정보
        google_api_info: dict = load(os.path.join(self.script_dir, '../resources/google_api_info.json'))
        # 스프레드시트 id
        self.spreadsheet_id: str = google_api_info['sheet_api']['spreadsheet_id'][service_mode]
        # 작업시트 이름
        self.rt_ranking_sheet: str = google_api_info['sheet_api']['work_sheet_name']['rt_ranking']
        self.gc_history_sheet: str = google_api_info['sheet_api']['work_sheet_name']['gc_history']
        # 구글 드라이브 id
        self.rt_ranking_id: str = google_api_info['drive_api']['drive_id']['rt_ranking']
        self.gc_history_id: str = google_api_info['drive_api']['drive_id']['gc_history']
        # 구글 API 인증키 json 파일 경로
        json_file_path: str = os.path.join(self.script_dir, f'../resources/{google_api_info['json_file_name']}') 
        
        # sheet API 서비스 셋팅
        self.sheet_service = sheet_api_settings(json_file_path, google_api_info['sheet_api']['scopes'])
        # drive API 서비스 셋팅
        self.drive_service = drive_api_settings(json_file_path, google_api_info['drive_api']['scopes'])
        
        # 로거 객체 생성
        self.logger = set_logger('/var/log/prasia_work/', 'prasia_work.log', 'prasia_work')
        
        # DB 연결
        self.mydb = connect_to_db()
        
    def update_ranking(self):
        """랭킹 정보 스프레드시트에 업데이트"""
        
        # update_ranking 함수 시작 로깅
        self.logger.info(f'----------Start ranking update----------')
        
        # 서버 리스트
        # e.g. [['LIVE_W01', 'LIVE_W01_R1', '아우리엘01'], ...]
        world_list: list = server_export()
        
        # 클래스 정보
        class_info: dict = load(os.path.join(self.script_dir, '../resources/class_info.json'))
        
        # 임시 테이블 생성
        query: str = """
        CREATE TEMPORARY TABLE temp_table (
            gcid VARCHAR(50) PRIMARY KEY,
            guild_id VARCHAR(50),
            ranking INT,
            world_name VARCHAR(50),
            gc_name VARCHAR(100),
            guild_name VARCHAR(100),
            class VARCHAR(50),
            grade VARCHAR(50),
            gc_level INT,
            update_time INT
        );
        """
        execute_query(self.mydb, query)

        # 모든 서버 캐릭터 정보 추가
        for world in world_list:
            # 랭킹 정보 호출
            ranking: list = ranking_call(world[0], world[1])
            
            # 캐릭터 정보 추가
            # e.g. [['1111500000000000164', '1311520000000000001', 1, '아우리엘1', '홍길동', '블랙', '환영검사', '16', 74], ...]
            for gc in ranking:
                # 결사 없으면 결사명 없음 처리(null로 되어 있어 데이터 업데이트가 안됨)
                if gc['guild_id'] == '0': gc['guild_name'] = '결사없음'
                
                # 임시 테이블에 데이터 INSERT
                query: str = f"""
                INSERT INTO
                    temp_table (gcid, guild_id, ranking, world_name, gc_name, guild_name, class, grade, gc_level, update_time)
                VALUES
                    ('{gc['gcid']}', '{gc['guild_id']}', {gc['ranking']}, '{world[2]}', '{gc['gc_name']}', '{gc['guild_name']}', '{class_info[gc['class']]}', '{gc['string_map']['grade']}', {gc['gc_level']}, UNIX_TIMESTAMP());
                """
                execute_query(self.mydb, query)
        
        # 이전 랭킹 테이블 데이터 확인
        query: str = """
        SELECT
            COUNT(*)
        FROM
            old_ranking;
        """
        query_result: int = execute_query(self.mydb, query)
        
        # 데이터가 없으면 INSERT(랭킹 히스토리 처리 에러 예외)
        if query_result[0][0] == 0:
            query: str = """
            INSERT INTO
                old_ranking SELECT * FROM temp_table;
            """
            execute_query(self.mydb, query)
            self.mydb.commit()
        
        # 업데이트 캐릭터 랭킹 정보
        query: str = """
        SELECT
            gcid, guild_id, ranking, world_name, gc_name, guild_name, class, grade, gc_level
        FROM
            temp_table
        ORDER BY
            world_name ASC, ranking ASC;
        """
        self.new_ranking: list = execute_query(self.mydb, query)
        
        # 신규 데이터 업데이트
        update_sheet(self.sheet_service, self.spreadsheet_id, self.rt_ranking_sheet, 'A3', self.new_ranking)
        
        # 시트 필터 리스트
        filterviews: list = filter_list(self.sheet_service, self.spreadsheet_id, self.rt_ranking_sheet)
        # 캐릭터 수만큼 필터 행 범위 설정
        gc_cnt: int = len(self.new_ranking)
        for filter in filterviews:
            # 필터 시작 행
            filter['range']['startRowIndex'] = 1
            # 필터 끝 행
            filter['range']['endRowIndex'] = gc_cnt + 2
        
        # 필터 행 범위 업데이트
        update_filter(self.sheet_service, self.spreadsheet_id, filterviews)
        
        # work_sheet 행 수
        row_num: int = row_cnt_sheet(self.sheet_service, self.spreadsheet_id, self.rt_ranking_sheet)
        # '기존 데이터 > 업데이트한 데이터'이면 초과되는 행 삭제
        if gc_cnt + 2 <= row_num:
            delete_values(self.sheet_service, self.spreadsheet_id, self.rt_ranking_sheet, f'A{gc_cnt+3}:I{row_num}')
    
        # 시간 업데이트
        self.time = current_time('%Y.%m.%d %H:%M:%S')
        update_sheet(self.sheet_service, self.spreadsheet_id, self.rt_ranking_sheet, 'O2', [[f'마지막 업데이트 : {self.time}']])

    def bakcup_data(self, drive_id: str, save_file_name: str, data):
        """데이터를 json 파일로 구글 드라이브에 백업"""
        
        # 백업 데이터 json 파일로 저장
        dump(save_file_name, data)
        
        # 백업 시간
        backup_time = current_time('%Y%m%d%H%M%S')
        # json 파일 구글 드라이브 업로드
        upload_drive(self.drive_service, drive_id, 'text/json', save_file_name, f'{save_file_name.split('.')[0]}_{backup_time}.json')
    
    def update_gc_history(self, new_ranking: dict, gc_infos: dict) -> None:
        """캐릭터 히스토리 업데이트 함수"""
        
        # update_gc_history 함수 시작 로깅
        self.logger.info(f'----------Start gc_history update----------')
        
        # 업데이트 시간
        update_time: str = current_time('%Y.%m.%d %H:%M:%S')
        # 유닉스 타임스탬프로 변환
        unix_time: int = convert_time_to_timestamp(update_time)
        
        # 캐릭터 히스토리 파일 로드
        gc_history_files: list = get_file_list('/save_data/gc_history/', 'gc_history*')
        
        # 이전 파일이 없으면 랭킹 히스토리 패스
        if not gc_history_files:
            # 이전 파일 없음 로깅
            self.logger.warning(f'gc_history update end because there is not file')
            # update_gc_history 함수 종료 로깅
            self.logger.info(f'----------End gc_history update----------')
            
            return

        # 최근 캐릭터 히스토리 파일
        last_gc_history_file: str = gc_history_files[0]
        # json 파일 > dict 변환
        self.gc_history: dict = load(last_gc_history_file)
        
        # 기존 ranking_view 초기화
        for gcid in self.gc_history['gc_info']: self.gc_history['gc_info'][gcid]['rangking_view'] = False
        
        # gc_infos 테이블 INSERT 데이터 저장 리스트
        gc_infos_data: list = []
        # gc_update_history 테이블 INSERT 데이터 저장 리스트
        gc_update_history_data: list = []
        
        # 시간 업데이트
        self.gc_history['update_time'] = update_time
        # 캐릭터 정보 업데이트
        for gcid in new_ranking:
            # 최신 캐릭터 정보 데이터 파싱
            new_gc_info: list = new_ranking[gcid]
            
            # gc_infos 테이블에서 캐릭 정보 확인
            gc_check = gc_infos.get(gcid)
            
            # 캐릭터 정보가 없으면 히스토리 추가
            if not gc_check: 
                self.gc_history['gc_info'][gcid] = {'history': []}
                # 스프레드시트 업데이트를 위해 gcid 키 추가
                gc_infos[gcid] = []
            
            # 캐릭터 변경사항 확인 후 히스토리 추가
            update_code: list = self.compare_gc_info(gc_check, new_gc_info)
            # 리스트 요소 0~2개
            for code in update_code:
                # 0: 신규추가, 1: 서버이전, 2: 결사탈퇴, 3: 결사가입, 4: 직업 변경
                # 마이그레이션 완료 후 gc_history 삭제 필요
                if code == 0:
                    # (2024.05.01 00:22:07) 신규추가/랭킹/서버/닉네임/결사/직업
                    self.gc_history['gc_info'][gcid]['history'].append(f'({update_time}) 신규추가/{new_gc_info[2]}위/{new_gc_info[3]}/{new_gc_info[4]}/{new_gc_info[5]}/{new_gc_info[6]}')
                elif code == 1:
                    # (2024.05.01 00:22:07) 서버이전/(old_랭킹 > new_랭킹)/(old_서버 > new_서버)/(old_닉네임 > new_닉네임)/(old_결사 > new_결사)/직업
                    self.gc_history['gc_info'][gcid]['history'].append(f'({update_time}) 서버이전/({gc_check[2]}위 > {new_gc_info[2]}위)/({gc_check[3]} > {new_gc_info[3]})/({gc_check[4]} > {new_gc_info[4]})/({gc_check[5]} > {new_gc_info[5]})/{new_gc_info[6]}')
                elif code == 2:
                    # (2024.05.01 00:22:07) old_결사 결사탈퇴
                    self.gc_history['gc_info'][gcid]['history'].append(f'({update_time}) {gc_check[5]} 결사탈퇴')
                elif code == 3:
                    # (2024.05.01 00:22:07) old_결사 > new_결사 결사가입
                    self.gc_history['gc_info'][gcid]['history'].append(f'({update_time}) {gc_check[5]} > {new_gc_info[5]} 결사가입')
                elif code == 4:
                    # (2024.05.01 00:22:07) old_클래스 > new_클래스 클래스변경
                    self.gc_history['gc_info'][gcid]['history'].append(f'({update_time}) {gc_check[6]} > {new_gc_info[6]} 클래스변경')
                
                # 신규추가는 최신 데이터만 INSERT
                if code == 0:
                    gc_update_history_data.append([gcid, None, None, None, None, None, None, None, None,
                                                   new_gc_info[1], new_gc_info[2], new_gc_info[3], new_gc_info[4], new_gc_info[5], new_gc_info[6], new_gc_info[7], new_gc_info[8],
                                                   code, unix_time, None
                                                ])
                # 나머지 업데이트는 이전, 최신 데이터 INSERT
                else:
                    gc_update_history_data.append([gcid, gc_check[1], gc_check[2], gc_check[3], gc_check[4], gc_check[5], gc_check[6], gc_check[7], gc_check[8],
                                                   new_gc_info[1], new_gc_info[2], new_gc_info[3], new_gc_info[4], new_gc_info[5], new_gc_info[6], new_gc_info[7], new_gc_info[8],
                                                   code, unix_time, None
                                                ])
            
            # 최신 정보로 업데이트
            self.gc_history['gc_info'][gcid]['guild_id'] = new_gc_info[1]
            self.gc_history['gc_info'][gcid]['ranking'] = new_gc_info[2]
            self.gc_history['gc_info'][gcid]['world_name'] = new_gc_info[3]
            self.gc_history['gc_info'][gcid]['gc_name'] = new_gc_info[4]
            self.gc_history['gc_info'][gcid]['guild_name'] = new_gc_info[5]
            self.gc_history['gc_info'][gcid]['class'] = new_gc_info[6]
            self.gc_history['gc_info'][gcid]['grade'] = new_gc_info[7]
            self.gc_history['gc_info'][gcid]['gc_level'] = new_gc_info[8]
            self.gc_history['gc_info'][gcid]['last_update'] = update_time
            self.gc_history['gc_info'][gcid]['rangking_view'] = True
            
            # 최신 업데이트 정보 추가
            new_gc_info.append(unix_time)
            gc_infos_data.append(new_gc_info)
            # 스프레드시트 업데이트를 위해 최신 정보로 업데이트
            gc_infos[gcid] = new_gc_info
        
        # 최신 캐릭터 정보 INSERT(gcid 레코드가 있으면 UPDATE)
        qeury: str = """
            INSERT INTO gc_infos (gcid, guild_id, ranking, world_name, gc_name, guild_name, class, grade, gc_level, update_time)
            VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
            ON DUPLICATE KEY UPDATE
            guild_id = VALUES(guild_id),
            ranking = VALUES(ranking),
            world_name = VALUES(world_name),
            gc_name = VALUES(gc_name),
            guild_name = VALUES(guild_name),
            class = VALUES(class),
            grade = VALUES(grade),
            gc_level = VALUES(gc_level),
            update_time = VALUES(update_time);
        """
        execute_query(self.mydb, qeury, seq_params=gc_infos_data)
        self.mydb.commit()
        
        # 캐릭터 히스토리 업데이트 정보가 있으면 INSERT
        if gc_update_history_data:
            qeury: str = """
                INSERT INTO gc_update_history
                (gcid, old_guild_id, old_ranking, old_world_name, old_gc_name, old_guild_name, old_class, old_grade, old_gc_level,
                new_guild_id, new_ranking, new_world_name, new_gc_name, new_guild_name, new_class, new_grade, new_gc_level,
                update_code, update_time, description)
                VALUES
                (%s, %s, %s, %s, %s, %s, %s, %s, %s,
                %s, %s, %s, %s, %s, %s, %s, %s,
                %s, %s, %s);
            """
            execute_query(self.mydb, qeury, seq_params=gc_update_history_data)
            self.mydb.commit()
        
        """ * 히스토리의 주요 목적은 변경사항이라고 판단되어 랭킹 OUT 제거 *
              (불필요한 랭킹 OUT 데이터가 너무 많이 발생)
        # 랭킹 OUT 캐릭터 있으면 히스토리 추가
        for gcid in old_ranking:
            # 최신 랭킹 정보에 캐릭터 정보가 없으면 랭킹 OUT
            gc_check = new_ranking.get(gcid)
            if not gc_check:
                # (시간) 랭킹OUT/랭킹/서버/닉네임/결사/직업
                self.gc_history['gc_info'][gcid]['history'].append(f'({update_time}) 랭킹OUT/{old_ranking[gcid][2]}위/{old_ranking[gcid][3]}/{old_ranking[gcid][4]}/{old_ranking[gcid][5]}/{old_ranking[gcid][6]}')
                # 랭킹OUT은 이전 데이터만 INSERT
                gc_update_history_data.append([gcid, old_ranking[gcid][1], old_ranking[gcid][2], old_ranking[gcid][3], old_ranking[gcid][4], old_ranking[gcid][5], old_ranking[gcid][6], old_ranking[gcid][7], old_ranking[gcid][8],
                                               None, None, None, None, None, None, None, None,
                                               "랭킹OUT", unix_time, None
                                            ])
        """
        
        # 스프레드시트 업데이트를 위해 캐릭터 히스토리 조회
        query = """
            SELECT
                gcid, description
            FROM 
                gc_update_history
            ORDER BY
                update_time ASC;
        """
        gc_update_history: list = execute_query(self.mydb, query)
        
        # gcid를 키로 히스토리 저장
        gcid_history: dict = {}
        for history in gc_update_history:
            # gcid 키가 없으면 추가
            gcid_key_check: list = gcid_history.get(history[0])
            if not gcid_key_check: gcid_history[history[0]] = []
            # 히스토리 추가
            gcid_history[history[0]].append(history[1])
        # 히스토리 join
        join_history: dict = {
            gcid: ' > '.join(gcid_history[gcid])
            for gcid in gcid_history
        }
        
        # 시트 업데이트 양식에 맞게 리스트로 저장
        sheet_data: list = [
            [
                # gcid, 서버, 닉네임, 결사, 마지막 업데이트, 히스토리
                gcid, gc_infos[gcid][3], gc_infos[gcid][4], gc_infos[gcid][5], convert_timestamp_to_time(gc_infos[gcid][9]), join_history[gcid]
            ]
            for gcid in gc_infos
        ]

        # 신규 데이터 업데이트
        update_sheet(self.sheet_service, self.spreadsheet_id, self.gc_history_sheet, 'A2', sheet_data)
        
        # 이전 랭킹 테이블 기존 데이터 삭제
        query: str = """
            TRUNCATE TABLE old_ranking;
        """
        execute_query(self.mydb, query)
        # 신규 데이터 INSERT
        query: str = """
            INSERT INTO
                old_ranking SELECT * FROM temp_table;
        """
        execute_query(self.mydb, query)
        self.mydb.commit()
        
        # update_gc_history 함수 종료 로깅
        self.logger.info(f'----------End gc_history update----------')
    
    def target_check(self, old_ranking: dict, new_ranking: dict, gc_infos: dict) -> None:
        """감시 대상 랭킹 In/Out 체크 함수"""
        
        # update_gc_history 함수 시작 로깅
        self.logger.info(f'----------Start target check----------')
        
        # 랭킹 In/Out 감시 대상 gcid get
        results: dict = get_values(self.sheet_service, self.spreadsheet_id, '감시 대상', 'A2:A')
        # 감시 대상이 없으면 함수 종료
        if not 'values' in results:
            # 감시 대상 없음 로깅
            self.logger.info(f'There are no monitoring targets')
            # update_gc_history 함수 종료 로깅
            self.logger.info(f'----------End target check----------')
            
            return None
        
        # 감시 대상 gcid
        targets_gcid: list = results['values']
        
        # 감시 대상 로깅
        self.logger.debug(f'Monitoring target : {targets_gcid}')
        
        # 이전 실시간 정보 로깅
        self.logger.debug(f'Old ranking : {old_ranking}')

        # 디스코드 정보 저장한 josn 파일 불러오기
        discord_info: dict = load(os.path.join(self.script_dir, '../resources/discord_info.json'))
        # 디스코트 감시 대상 모니터링 채널 url
        discord_url: str = discord_info['monitoring_url']
        
        # 감시 대상 변경사항 체크 후에 디스코드에 전송할 메시지 저장할 리스트
        messages: list = [[] for _ in range(5)]
        
        # 감시 대상 변경사항 확인
        for gcid in targets_gcid:
            # 이전 랭킹 정보에서 캐릭터 정보 확인
            old_gc_check = old_ranking.get(gcid[0])
            # 최신 랭킹 정보에서 캐릭터 정보 확인
            new_gc_check = new_ranking.get(gcid[0])
            # 히스토리 파일에서 캐릭터 정보 확인
            gc_infos_check = gc_infos.get(gcid[0])

            # 이전 랭킹에 캐릭터 정보가 있고, 최신 랭킹에 캐릭터 정보가 없으면 랭킹 OUT
            if old_gc_check and not new_gc_check:
                # 디스코트 전송할 메시지: (gcid) 랭킹OUT/랭킹/서버/닉네임/결사/직업
                send_text: str = f'({gcid[0]}) 랭킹OUT/{old_gc_check[2]}위/{old_gc_check[3]}/{old_gc_check[4]}/{old_gc_check[5]}/{old_gc_check[6]}'
                # 리스트에 추가
                messages[0].append(send_text)
                continue
            
            try:
                # 캐릭터 정보가 없으면 예외 처리
                if not gc_infos_check: raise GcidNotFound(gcid[0])
            # 누적 정보에 추적 캐릭터 정보가 없으면 건너뛰기
            except GcidNotFound as e:
                self.logger.warning(f'An warning occurred: {e}', exc_info=True)
                continue
            
            # 캐릭터 변경사항 확인
            update_code: list = self.compare_gc_info(gc_infos_check, new_gc_check)
            # 리스트 요소 0~2개
            for code in update_code:
                # 1: 서버이전, 2: 결사탈퇴, 3: 결사가입, 4: 랭킹없음
                if code == 1:
                    # 디스코트 전송할 메시지: (gcid) 서버이전/(old_랭킹 > new_랭킹)/(old_서버 > new_서버)/(old_닉네임 > new_닉네임)/(old_결사 > new_결사)/직업
                    send_text: str = f'({gcid[0]}) 서버이전/({gc_infos_check[2]}위 > {new_gc_check[2]}위)/({gc_infos_check[3]} > {new_gc_check[3]})/({gc_infos_check[4]} > {new_gc_check[4]})/({gc_infos_check[5]} > {new_gc_check[5]})/{new_gc_check[6]}'
                    messages[1].append(send_text)
                elif code == 2:
                    # 디스코트 전송할 메시지: (gcid) 결사탈퇴/new_랭킹/new_서버/new_닉네임/(old_결사 > new_결사)/직업
                    send_text: str = f'({gcid[0]}) 결사탈퇴/{new_gc_check[2]}위/{new_gc_check[3]}/{new_gc_check[4]}/({gc_infos_check[5]} > {new_gc_check[5]})/{new_gc_check[6]}'
                    messages[2].append(send_text)
                elif code == 3:
                    # 디스코트 전송할 메시지: (gcid) 결사가입/new_랭킹/new_서버/new_닉네임/new_결사/직업
                    send_text: str = f'({gcid[0]}) 결사가입/{new_gc_check[2]}위/{new_gc_check[3]}/{new_gc_check[4]}/{new_gc_check[5]}/{new_gc_check[6]}'
                    messages[3].append(send_text)
                elif code == 4:
                    self.logger.info(f'There is no {gc_infos_check[4]}({gcid[0]}) in the ranking.')
        
        # 각 업데이트 정보 텍스트 변환 후 디스코드 채널로 메시지 전송
        for i in range(len(messages)):
            send_message = '\n'.join(messages[i])
            # 메시지가 있으면 전송
            if len(send_message):
                send(discord_url, send_message)
            # 없으면 로깅
            else:
                self.logger.info(f'code {i}: No updates.')
        
        # target_check 함수 종료 로깅
        self.logger.info(f'----------End target check----------')

    def compare_gc_info(self, old_gc_info: list, new_gc_info: list) -> list:
        """
        누적된 히스토리 데이터에서 gc_info 정보를 비교해 변경사항 반환하는 함수
        [반환 코드] 0: 신규추가, 1: 서버이전, 2: 결사탈퇴, 3: 결사가입, 4: 직업 변경
        """
        
        # 비교 정보 로깅
        self.logger.debug(f'old_gc_info: {old_gc_info}')
        self.logger.debug(f'new_gc_info: {new_gc_info}')
        
        # 비교 결과
        result: list = []
        
        # 누적 정보에 최신 정보가 없으면 신규 추가
        if not old_gc_info:
            result.append(0)
            
            return result
        # 최신 정보가 없으면 랭킹에 없음
        elif not new_gc_info:
            result.append(4)
            
            return result
        # 이전 정보와 최신 정보의 서버가 다르면 서버이전
        elif old_gc_info[3] != new_gc_info[3]:
            result.append(1)
        # 이전 정보와 최신 정보의 서버가 같은데 닉네임이 다르면 10분 안에 서버이전 왕복
        elif old_gc_info[3] == new_gc_info[3] and old_gc_info[4] != new_gc_info[4]:
            result.append(1)
        # guild_id가 달라졌는데 0이면 결사 탈퇴
        elif old_gc_info[1] != new_gc_info[1] and new_gc_info[1] == '0':
            result.append(2)
        # guild_id가 달라졌는데 0이 아니면 결사 가입
        elif old_gc_info[1] != new_gc_info[1] and new_gc_info[1] != '0':
            result.append(3)

        # 이전 정보와 최신 정보의 직업이 다르면 직업변경
        if old_gc_info[6] != new_gc_info[6]:
            # 직업변경 / 랭킹 / 서버 / 닉네임 / 결사 / (old_직업 > new_직업)
            result.append(4)
        
        return result