# 표준 라이브러리
import sys
# 설치 모듈
import requests
# 프로젝트 모듈
from src.utils.my_logging import set_logger
# 예외 처리 에러
from src.error_make import RankingCallFailed

# 로거 객체 생성
logger = set_logger('/var/log/prasia_work/', 'prasia_work.log', 'prasia_work')

def ranking_call(world_group_id: str, world_id: str) -> list:
    """랭킹 api 호출 반환 함수"""
    
    # 랭킹 api 호출 서버 로깅
    logger.info(f'Ranking call : {world_id}')
    
    # api 요청 헤더
    headers: dict = {
    'Authorization': 'Bearer 3E5C4287-849D-4EAA-A4EB-4528C9AB8E7D',
    'x-wp-api-key': 'wp_fe_api_key'
    }
    # 서버 정보
    params: dict = {
        'world_group_id': world_group_id,
        'world_id': world_id
    }
    # 랭킹 api 호출
    ranking: dict = requests.post('https://wp-api.nexon.com/v1/GameData/gcranking', headers=headers, json=params).json()
    
    try:
        if ranking['message'] != 'SUCCESS':
            raise RankingCallFailed
    except RankingCallFailed as e:
        logger.error(f'An error occurred: {ranking['message']}')
        sys.exit(1)
    
    # 서버 랭킹 로깅
    logger.debug(f'{world_id} Ranking : {ranking['result']['gc']}')

    return ranking['result']['gc']

if __name__ == '__main__':

    # 아우리엘 1섭 랭킹 호출
    ranking: list = ranking_call('LIVE_W01', 'LIVE_W01_R1')
    
    for gc in ranking:
        print(gc)