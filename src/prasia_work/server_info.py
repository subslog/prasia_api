# 프로젝트 모듈
from src.utils.my_logging import set_logger

world_id_list: dict = {
    'LIVE_W01': [
        '아우리엘',['_R1', '_R4', '_R5']
    ],
    'LIVE_W02': [
        '론도', ['_R1', '_R2', '_R3', '_R4', '_R5']
    ],
    'LIVE_W03': [
        '라인소프', ['_R1', '_R2', '_R3', '_R5']
    ],
    'LIVE_W04': [
        '시길', ['_R1', '_R2', '_R4']
    ],
    'LIVE_W05': [
        '아민타', ['_R1', '_R2', '_R3', '_R5']
    ],
    'LIVE_W07': [
        '이오스', ['_R1', '_R2', '_R3', '_R4']
    ],
    'LIVE_W08': [
        '가리안', ['_R3', '_R4', '_R5']
    ],
    'LIVE_W09': [
        '벨세이즈', ['_R1', '_R3', '_R4']
    ],
    'LIVE_W10': [
        '사도바', ['_R1', '_R2', '_R4', '_R5']
    ],
    'LIVE_W11': [
        '제롬', ['_R1', '_R2', '_R4', '_R5']
    ],
    'LIVE_W12': [
        '아티산', ['_R1', '_R2', '_R4']
    ],
    'LIVE_W13': [
        '엘렌', ['_R1', '_R2', '_R4']
    ],
    'LIVE_W14': [
        '나세르', ['_R1', '_R2', '_R3', '_R5']
    ],
    'LIVE_W15': [
        '필레츠', ['_R1', '_R3', '_R5']
    ],
    'LIVE_W16': [
        '타리아', ['_R1', '_R2', '_R3', '_R4', '_R5']
    ],
    'LIVE_W17': [
        '카렐', ['_R1', '_R2', '_R4', '_R5']
    ],
    'LIVE_W18': [
        '나스카', ['_R1', '_R2', '_R3']
    ],
    'LIVE_W19': [
        '벤아트', ['_R1', '_R2', '_R3', '_R4']
    ],
    'LIVE_W20': [
        '페넬로페', ['_R1', '_R2', '_R3']
    ],
    'LIVE_W21': [
        '마커스', ['_R1', '_R2', '_R3', '_R4', '_R5', '_R6', '_R7']
    ],
}

# 로거 객체 생성
logger = set_logger('/var/log/prasia_work/', 'prasia_work.log', 'prasia_work')

def server_export() -> list:
    """[서버 그룹 id, 서버 id, 서버 이름] 형태의 리스트로 반환"""
    
    world_list = []
    
    for group_id in world_id_list:
        # 서버 그룹 이름
        world_group_name = world_id_list[group_id][0]
        # 서버 id, 서버 이름 추가
        for r_num in world_id_list[group_id][1]:
            # 서버 id
            world_id = group_id + r_num
            # 서버 이름
            world_name = world_group_name + r_num[-1].zfill(2)
            
            world_list.append([group_id, world_id, world_name])
        
    # 서버 갯수
    logger.info(f'Server count : {len(world_list)}')
    # 서버 리스트
    logger.debug(f'Server list : {world_list}')
    
    return world_list

if __name__ == '__main__':

    # 서버 리스트
    world_list = server_export()
    
    # 서버 리스트 출력
    for world in world_list:
       print(world)