# 표준 라이브러리
import time
import sys
from datetime import datetime
# 프로젝트 모듈
from src.utils.my_logging import set_logger

# 로거 객체 생성
logger = set_logger('/var/log/prasia_work/', 'prasia_work.log', 'prasia_work')

def current_time(time_f: str) -> str:
    """현재 시간 반환 함수"""
    
    try:
        # 현재 시간
        now = time.strftime(time_f)
    except TypeError as e:
        logger.error(f'An error occurred: {e}', exc_info=True)
        sys.exit(1)
    
    # 시간 로깅
    logger.info(f'Current time : {now}')
    
    return now

def convert_time_to_timestamp(time_string: str, format: str='%Y.%m.%d %H:%M:%S') -> int:
    """문자열 형식의 시간을 유닉스 타임스탬프로 변환 후 반환하는 함수"""
    
    # 문자열을 datetime 객체로 변환
    dt = datetime.strptime(time_string, format)
    # datetime 객체를 유닉스 타임스탬프로 변환
    unix_timestamp: int = int(dt.timestamp())
    
    return unix_timestamp

def convert_timestamp_to_time(timestamp: int, format: str='%Y.%m.%d %H:%M:%S') -> str:
    """유닉스 타임스탬프 형식의 시간을 문자열 형식의 시간으로 변환 후 반환하는 함수"""
    
    # 유닉스 타임스탬프 > 현재 시간 변환
    current_time = datetime.fromtimestamp(timestamp)
    # 문자열을 datetime 객체로 변환
    time_string: str = current_time.strftime(format)

    return time_string

if __name__ == '__main__':
    print(current_time('%Y.%m.%d %H:%M:%S'))