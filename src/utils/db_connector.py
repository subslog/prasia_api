# 표준 라이브러리
import os
import sys
# 설치 모듈
import mysql.connector
# 프로젝트 모듈
from src.utils.my_logging import set_logger
from src.utils.os_work import load_file
# 클래스
from mysql.connector.connection_cext import CMySQLConnection
from mysql.connector.cursor_cext import CMySQLCursor

# 로거 객체 생성
logger = set_logger('/var/log/prasia_work/', 'prasia_work.log', 'prasia_work')

def connect_to_db():
    """DB 연결 함수"""
    
    # 환경변수에서 DB 연결 정보 읽기
    MYSQL_HOST = load_file(os.getenv('MYSQL_HOST'))
    MYSQL_PORT = load_file(os.getenv('MYSQL_PORT'))
    MYSQL_USER = load_file(os.getenv('MYSQL_USER'))
    MYSQL_PASSWORD = load_file(os.getenv('MYSQL_PASSWORD'))
    MYSQL_DATABASE = load_file(os.getenv('MYSQL_DATABASE'))
    
    # DB 연결 시작 로깅
    logger.info('----------Start DB connect----------')
    # DB 접속 정보 로킹
    logger.debug(f'DB Info : {MYSQL_DATABASE}, {MYSQL_USER}@{MYSQL_HOST}:{MYSQL_PORT}')
    
    try:
        # MySQL 서버에 연결
        mydb: CMySQLConnection = mysql.connector.connect(
            host=MYSQL_HOST,
            port=MYSQL_PORT,
            user=MYSQL_USER,
            password=MYSQL_PASSWORD,
            database=MYSQL_DATABASE,
            connect_timeout=10
        )
    except mysql.connector.errors.DatabaseError as e:
        logger.error(f'An error occurred: {e}', exc_info=True)
        sys.exit(1)
    except TypeError as e:
        logger.error(f'An error occurred: {e}', exc_info=True)
        sys.exit(1)
    
    # DB 연결 성공 로깅
    logger.info('----------DB connection successful----------')
        
    return mydb

def execute_query(mydb: CMySQLConnection, query: str, seq_params: list=[]):
    """mydb DB로 query 수행 함수"""
    
    # 쿼리문 로깅
    logger.debug(f'Request query : {query}')
    
    # 커서 생성
    mycursor: CMySQLCursor = mydb.cursor()
    
    try:
        # 쿼리 실행
        if seq_params:
            mycursor.executemany(query, seq_params)
        else:
            mycursor.execute(query)
    except mysql.connector.errors.ProgrammingError as e:
        logger.error(f'An error occurred: {e}', exc_info=True)
        sys.exit(1)
    except mysql.connector.errors.IntegrityError as e:
        logger.error(f'An error occurred: {e}', exc_info=True)
        sys.exit(1)
    except mysql.connector.errors.InterfaceError as e:
        logger.error(f'An error occurred: {e}', exc_info=True)
        sys.exit(1)
    except mysql.connector.errors.DataError as e:
        logger.error(f'An error occurred: {e}', exc_info=True)
        sys.exit(1)
    
    # 쿼리 실행 결과
    result: list = mycursor.fetchall()
    
    # 커서 연결 종료
    mycursor.close()

    return result