# 표준 라이브러리
import os
import glob
import sys
# 프로젝트 모듈
from src.utils.my_logging import set_logger
from src.utils.json_work import dump
from src.utils.date import current_time

# 로거 객체 생성
logger = set_logger('/var/log/prasia_work/', 'prasia_work.log', 'prasia_work')

def dict_to_json(save_path: str, save_name: str, data: dict):
    """dict 데이터를 json 파일로 저장"""
    
    try:
        # 저장 경로 없으면 생성
        if not os.path.isdir(save_path):
            os.makedirs(save_path)
            # 저장 경로 생성 로깅
            logger.warning(f'Created because {save_path} path could not be found.')
    except TypeError as e:
        logger.error(f'An error occurred: {e}', exc_info=True)
        sys.exit(1)
    
    # 저장 시간
    save_time = current_time('%Y%m%d%H%M%S')
    
    try:
        # 저장할 파일명
        save_name: str = f'{save_name.split('.')[0]}_{save_time}.json'
    except AttributeError as e:
        logger.error(f'An error occurred: {e}', exc_info=True)
        sys.exit(1)
        
    # 저장 경로 설정
    save_file_path: str = os.path.join(save_path, save_name)
    
    # 저장 경로 로깅
    logger.info(f'Save file path : {save_file_path}')
    
    # json 파일 저장
    dump(save_file_path, data)

def get_file_list(directory_path: str, pattern: str='*') -> list:
    """directory_path 경로에서 pattern에 매칭되는 파일 리스트 반환"""
    
    # 검색 정보 로깅
    logger.info(f'Search path : {directory_path}')
    logger.info(f'Matching pattern : {pattern}')
    
    # pattern에 매칭되는 파일 리스트
    files = glob.glob(os.path.join(directory_path, pattern))
    
    # 생성된 시간 순으로 정렬
    files.sort(key=os.path.getctime, reverse=True)
    
    # 매칭된 파일 리스트 로깅
    logger.debug(f'Matching file list : {files}')
    
    return files

def limit_files(file_path: str, max_files: int, file_type: str):
    """file_path 경로의 file_type 확장자 파일 max_files 수 초과하는 파일 삭제"""
    
    # 파일 제한 시작 로깅
    logger.info('----------Start file limit----------')
    
    # 검색 정보 로깅
    logger.info(f'Search path : {file_path}')
    logger.info(f'Max files : {max_files}')
    logger.info(f'File type : {file_type}')
    
    try:
        # file_path 경로에 있는 file_type 타입의 파일 리스트
        files = [os.path.join(file_path, file) for file in os.listdir(file_path) if file.endswith(f'.{file_type}')]
    except FileNotFoundError as e:
        logger.error(f'An error occurred: {e}', exc_info=True)
        sys.exit(1)
    except OSError as e:
        logger.error(f'An error occurred: {e}', exc_info=True)
        sys.exit(1)
    
    try:
        if len(files) > max_files:
            # 파일 생성 날짜로 오름차순
            # 최대 보관 파일 수를 초과하는 파일만 추출
            old_files = sorted(files, key=os.path.getctime)[:len(files) - max_files]
            
            # 최대 보관 파일 수 초과한 파일 삭제
            for old_file in old_files: os.remove(old_file)
                
            # 삭제한 파일 리스트 로깅
            logger.debug(f'Old files : {old_files}')
    except TypeError as e:
        logger.error(f'An error occurred: {e}', exc_info=True)
        sys.exit(1)
    

    # 파일 제한 종료 로깅
    logger.info('----------End file limit----------')

def load_file(file_path: str):
    """파일을 읽어서 반환하는 함수"""
    
    try:    
        with open(file_path, 'r', encoding='UTF-8') as f:
            data = f.readline().rstrip('\n')
    except FileNotFoundError as e:
        logger.error(f'An error occurred: {e}', exc_info=True)
        sys.exit(1)

    return data

if __name__ == '_main_':
    # dict_to_json 테스트
    test_dict = {'test': 'test'}
    dict_to_json('/tmp', 'test.json', test_dict)