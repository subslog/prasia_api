# 표준 라이브러리
import configparser

def read_config(config_path: str):
    """conf 파일을 읽어 configparser 객체로 반환"""
    
    # configparser 객체 생성
    config = configparser.ConfigParser()
    # config 파일 read
    config.read(config_path, encoding='utf-8')
    
    return config