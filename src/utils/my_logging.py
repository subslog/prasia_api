# 표준 라이브러리
import logging
from logging.handlers import RotatingFileHandler
import os
import sys
# 프로젝트 모듈
from src.utils.config_reader import read_config

class ContextFilter(logging.Filter):
    """필터를 통해 로그 레코드에 pid를 추가"""
    def filter(self, record):
        record.pid = os.getpid()
        return True

def set_logger(save_path: str, log_file_name: str, logger_name: str) -> logging.Logger:
    """로거 객체를 반환하는 함수"""
    
    # 현재 스크립트 절대경로
    script_dir = os.path.dirname(os.path.abspath(__file__))
    
    # conf 파일 read
    logging_config = read_config(os.path.join(script_dir, '../../conf/prasia_work.conf'))['Logging']

    # 로그 저장 경로 설정
    log_file_paht: str = os.path.join(save_path, log_file_name)

    # 호출한 모듈의 함수 이름으로 로거 생성
    logger = logging.getLogger(logger_name)
    # 로그 레벨 설정
    logger.setLevel(logging_config['level'])

    # 이미 로거에 핸들러가 존재하면 기존 핸들러 반환
    if logger.handlers: return logger

    # 파일 출력 핸들러 추가(최대 크기 10MB, 최대 파일 수 10개)
    file_handler = RotatingFileHandler(log_file_paht,  encoding='utf-8', maxBytes=10000000, backupCount=10)

    # 로그 포맷 지정 [2024/05/06 18:33:00][level] log text
    formatter = logging.Formatter('[%(asctime)s][%(levelname)s][%(module)s.py-%(funcName)s()][pid:%(pid)s] %(message)s', datefmt='%Y/%m/%d %H:%M:%S')
    file_handler.setFormatter(formatter)
    
    # 로거에 필터 추가
    logger.addFilter(ContextFilter())
    
    # 로거에 핸들러 추가
    logger.addHandler(file_handler)
    
    # 현재 스크립트 절대경로
    script_dir = os.path.dirname(os.path.abspath(__file__))
    try:
        # 로그 저장 경로 없으면 생성
        if not os.path.isdir(save_path):
            os.makedirs(save_path)
            # 저장 경로 생성 로깅
            logger.warning(f'Created because {save_path} path could not be found.')
    except TypeError as e:
        logger.error(f'An error occurred: {e}', exc_info=True)
        sys.exit(1)

    return logger