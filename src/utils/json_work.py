# 표준 라이브러리
import json
import sys
# 프로젝트 모듈
from src.utils.my_logging import set_logger

# 로거 객체 생성
logger = set_logger('/var/log/prasia_work/', 'prasia_work.log', 'prasia_work')

def load(json_file_path: str) -> dict:
    """json 파일을 읽어 dcit 형태로 반환"""
    
    try:
        # json 파일 읽기
        with open(json_file_path, 'r', encoding='UTF-8') as f:
            json_data = json.load(f)
    except FileNotFoundError as e:
        logger.error(f'An error occurred: {e}', exc_info=True)
        sys.exit(1)
    except json.decoder.JSONDecodeError as e:
        logger.error(f'An error occurred: {e}', exc_info=True)
        sys.exit(1)
    
    # json 데이터 로깅
    logger.debug(f'Json data : {json_data}')
        
    return json_data

def loads(json_text: str) -> dict:
    """json 형태의 텍스트 파일을 dcit 형태로 반환"""
    
    json_data = json.loads(json_text)
        
    return json_data

def dump(json_file_path: str, data):
    """data > json 파일 저장 함수"""
    
    try:
        # json 파일로 저장
        with open(json_file_path, 'w', encoding='UTF-8') as f:
            json.dump(data, f, indent=4, ensure_ascii=False)
    except PermissionError as e:
        logger.error(f'An error occurred: {e}', exc_info=True)
        sys.exit(1)
    
if __name__ == '__main__':
    import os
    
    # 현재 스크립트의 상위 디렉터리 경로
    parent_dir = os.path.dirname(os.path.dirname(__file__))
    
    print(load(f'{parent_dir}/resources/update_rangking.json'))