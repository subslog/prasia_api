# 프로젝트 모듈
from src.utils.db_connector import connect_to_db
from src.utils.db_connector import execute_query
from src.utils.my_logging import set_logger

# 로거 객체 생성
logger = set_logger('/var/log/prasia_work/', 'prasia_work.log', 'prasia_work')

def start_task(task_name: str) -> None:
    """작업 시작 상태 표시 함수"""
    
    # 작업 상태 변경 로깅
    logger.info('Task state change : 0 > 1')
    
     # DB 연결
    mydb = connect_to_db()
    
    # 작업 시작 업데이트
    query: str = f"""
    UPDATE tasks
    SET status = 1
    WHERE task = '{task_name}'
    """
    execute_query(mydb, query)
    mydb.commit()
    
    # DB 연결 종료
    mydb.close()

def complete_task(task_name: str) -> None:
    """작업 완료 상태 표시 함수"""
    
    # 작업 상태 변경 로깅
    logger.info('Task state change : 1 > 0')
    
     # DB 연결
    mydb = connect_to_db()
    
    # 작업 시작 업데이트
    query: str = f"""
    UPDATE tasks
    SET status = 0
    WHERE task = '{task_name}'
    """
    execute_query(mydb, query)
    mydb.commit()
    
    # DB 연결 종료
    mydb.close()
