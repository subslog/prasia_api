# 표준 라이브러리
from datetime import datetime, timedelta
import time
import argparse
# 설치 모듈
from apscheduler.schedulers.background import BackgroundScheduler
# 프로젝트 모듈
from src.prasia_work import Prasia_work
from src.utils.os_work import dict_to_json
from src.utils.os_work import limit_files
from src.utils.my_logging import set_logger
from src.utils.config_reader import read_config
from src.utils.task_status import start_task
from src.utils.task_status import complete_task
from src.utils.db_connector import execute_query

# 로거 객체 생성
logger = set_logger('/var/log/prasia_work/', 'prasia_work.log', 'prasia_work')

def main(service_mode: str):
    """메인 실행 함수"""
    
    # 메임 함수 시작 로깅
    logger.info(f'----------Start main function----------')
    
    # 작업 시작 상태 변경
    start_task('prasia_work')
    
    # 프라시아 랭킹 정보 객체 생성
    work = Prasia_work(service_mode)
    
    # 실시간 랭킹 업데이트
    work.update_ranking()
    
    # 이전 실시간 랭킹 정보 조회
    query = """
    SELECT
        gcid, guild_id, ranking, world_name, gc_name, guild_name, class, grade, gc_level
    FROM
        old_ranking;
    """
    old_ranking: list = execute_query(work.mydb, query)
    # gcid를 키로 dict 타입 변환
    old_ranking_dict: dict = {gc_info[0]: list(gc_info) for gc_info in old_ranking}
    
    # 최신 실시간 랭킹 정보 gcid를 키로 dict 타입 변환
    new_ranking_dict: dict = {gc_info[0]: list(gc_info) for gc_info in work.new_ranking}
    
    # 누적된 gc_info 조회
    query = """
    SELECT
        gcid, guild_id, ranking, world_name, gc_name, guild_name, class, grade, gc_level, update_time
    FROM
        gc_infos;
    """
    gc_infos: list = execute_query(work.mydb, query)
    # gcid를 키로 dict 타입 변환
    gc_infos_dict: dict = {gc_info[0]: list(gc_info) for gc_info in gc_infos}
    
    # 감시 대상 랭킹 In/Out 확인
    work.target_check(old_ranking_dict, new_ranking_dict, gc_infos_dict)

    # 캐릭터 히스토리 업데이트
    work.update_gc_history(new_ranking_dict, gc_infos_dict)
    
    # 저장할 데이터
    backup_data: dict = {
        'update_time': work.time,
        'gc_info': new_ranking_dict
    }
    # 랭킹 정보 저장 경로
    save_path: str = '/save_data/ranking/'
    # 랭킹 정보 저장
    save_file_name: str = 'ranking_info.json'
    dict_to_json(save_path, save_file_name, backup_data)
    # 7개 초과하는 파일 삭제
    limit_files(save_path, 7, 'json')

    # 캐릭터 히스토리 저장 경로
    save_path: str = '/save_data/gc_history/'
    # 캐릭터 히스토리 저장
    save_file_name: str = 'gc_history.json'
    dict_to_json(save_path, save_file_name, work.gc_history)
    # 7개 초과하는 파일 삭제
    limit_files(save_path, 7, 'json')
    
    # DB 접속 종료
    work.mydb.close()
    
    # 작업 완료 상태 변경
    complete_task('prasia_work')

    # 메임 함수 종료 로깅
    logger.info(f'----------End main function----------')
    
def interval_job(service_mode: str):
    """현재 시간 +1 분에 스케쥴링하는 함수"""
    
    # 현재 시간 +1 분
    run_time = datetime.now() + timedelta(minutes=1)

    # interval_job 시작 로깅
    logger.info(f'----------Start main function at {run_time}----------')

    # 현재 시간 +1 분에 main 함수 실행
    scheduler.add_job(main, 'date', run_date=run_time, args=[service_mode], id='main')

if __name__ == '__main__':
    
    # ArgumentParser 객체 생성
    parser = argparse.ArgumentParser(description=f'This is a service that utilizes WARS OF PRASIA ranking information through API.')
    # 서비스 모드 Arg 추가
    parser.add_argument('--service_mode', dest='service_mode',
                        type=str,
                        choices=['prod', 'stg', 'dev'],
                        help='Set the service execution mode: prod or stg or dev',
                        action='store',
                        required=True)
    # 인수 파싱
    args = parser.parse_args()
    
    # 랭킹 정보 수집 시작
    logger.info(f'----------Start ranking information collection----------')
    # 서비스 모드 로깅
    logger.info(f'Service mode: {args.service_mode}')    
    # conf 정보 로깅
    config = read_config('conf/prasia_work.conf')
    config_text = [{section: config.items(section)} for section in config.sections()]
    logger.info(f'Configuration : {config_text}')
    
    # 프로그램 시작 시 바로 main 함수 실행
    main(args.service_mode)
    
    # 서비스 모드가 prod or stg 이면 스케쥴러 등록
    if args.service_mode in ['prod', 'stg']:
        # 스케쥴러 객체 생성
        scheduler = BackgroundScheduler()
        
        # interval_job 함수 10분마다 스케쥴링 추가
        scheduler.add_job(interval_job, 'cron', minute='*/10', args=[args.service_mode], id='interval_job')
        
        # 스케쥴러 시작
        scheduler.start()
        
        # 스케쥴러 유지를 위해 while
        while True: time.sleep(2)