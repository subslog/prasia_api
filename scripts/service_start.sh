#/bin/bash

source docker_commander.sh

# 옵션 체크 함수
check_parameters() {
  # 초기값
  local opt
  database=""
  user=""
  password=""

  # 맨 앞에 :을 붙여 getopts가 에러를 처리하지 않고 직접 처리
  # e.g. 유형 별로 opt, OPTARG 변수에 저장되는 값
  # -D arg1 : opt='D', OPTARG='값'
  # -D : opt=':', OPTARG='D'
  # 옵션 사용 안함 : opt='?', OPTARG=''

  while getopts ":D:u:p:f:s:" opt; do
    case $opt in
      D)
        # D 옵션 값 저장(데이터베이스 이름)
        database=$OPTARG
        ;;
      u)
        # u 옵션 값 저장(DB 계정)
        user=$OPTARG
        ;;
      p)
        # p 옵션 값 저장(DB 계정 패스워드)
        password=$OPTARG
        ;;
      f)
        # f 옵션 값 저장(도커 컴포즈 파일)
        docker_compose=$OPTARG
        ;;
      s)
        # s 옵션 값 저장(스택 이름)
        stack=$OPTARG
        ;;
      \?)
        # 잘못된 옵션 사용
        echo "Unknown option argument: -$OPTARG" >&2
        exit 1
        ;;
      :)
        # 옵션에 값이 비어있음
        echo "Option -$OPTARG requires a value" >&2
        exit 1
        ;;
    esac
  done

  # 필수 옵션이 없으면 예외 처리
  if [ -z $database ] || [ -z $user ] || [ -z $password ] || [ -z $docker_compose ] || [ -z $docker_compose ] || [ -z $stack ]; then
    echo "Error: All options are required" >&2
    exit 1
  fi
}

main() {
  # 파라미터 체크 및 변수 처리
  check_parameters $@

  # DB 서비스 컨테이너 ID
  container_id=$(get_container_id registry.gitlab.com/subslog/prasia_work:db_latest)

  i=1
  while :
  do
    # 서비스 동작 상태
    service_status=$(exec_container $container_id "mysql -D $database -u $user -p$password -s -e \"SELECT status FROM tasks WHERE task='prasia_work';\" 2> /dev/null")
  
    # 결과값이 없으면 예외 처리
    if [ -z $service_status ]; then
      echo "Error: There are no DB results" >&2
      exit 1
    # 서비스가 동작하지 않으면 배포
    elif [ $service_status == 0 ]; then
      echo ""
      echo "Start service deploy"
      # 기존 서비스 종료
      docker stack rm $stack
      # 최신 서비스 배포
      docker stack deploy -c ~/$docker_compose $stack
      break
    fi

    # 커서를 줄의 맨 앞으로 이동하고 현재 줄 삭제
    printf "\r\033[K"
    echo -n "Waiting for service to end"
    if ((i % 3 == 1)); then
        echo -n "."
    elif ((i % 3 == 2)); then
        echo -n ".."
    else
        echo -n "..."
    fi

    # i +1
    ((i++))

    sleep 1
  done
}

main $@