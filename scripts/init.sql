-- 스테이징 데이터베이스 생성
CREATE DATABASE IF NOT EXISTS stg_prasia_work;
-- 운영 데이터베이스 생성
CREATE DATABASE IF NOT EXISTS prasia_work;

-- 기존 생성된 prasia_work 계정의 패스워드 해시값
SET @user_password = (SELECT authentication_string FROM mysql.user WHERE host = '%' and user = 'prasia_work');

-- localhost, 172.16.0.% 호스트 계정 생성
CREATE USER 'prasia_work'@'localhost';
CREATE USER 'prasia_work'@'172.16.0.%';
UPDATE mysql.user SET authentication_string = @user_password WHERE host = 'localhost' and user = 'prasia_work';
UPDATE mysql.user SET authentication_string = @user_password WHERE host = '172.16.0.%' and user = 'prasia_work';

-- 데이터베이스 권한 부여
GRANT ALL PRIVILEGES ON stg_prasia_work.* TO 'prasia_work'@'localhost';
GRANT ALL PRIVILEGES ON stg_prasia_work.* TO 'prasia_work'@'172.16.0.%';
GRANT ALL PRIVILEGES ON prasia_work.* TO 'prasia_work'@'localhost';
GRANT ALL PRIVILEGES ON prasia_work.* TO 'prasia_work'@'172.16.0.%';

-- old_ranking 테이블 생성
CREATE TABLE IF NOT EXISTS stg_prasia_work.old_ranking (
    gcid VARCHAR(50) PRIMARY KEY,
    guild_id VARCHAR(50),
    ranking INT,
    world_name VARCHAR(50),
    gc_name VARCHAR(100),
    guild_name VARCHAR(100),
    class VARCHAR(50),
    grade VARCHAR(50),
    gc_level INT,
    update_time INT
);
CREATE TABLE IF NOT EXISTS prasia_work.old_ranking (
    gcid VARCHAR(50) PRIMARY KEY,
    guild_id VARCHAR(50),
    ranking INT,
    world_name VARCHAR(50),
    gc_name VARCHAR(100),
    guild_name VARCHAR(100),
    class VARCHAR(50),
    grade VARCHAR(50),
    gc_level INT,
    update_time INT
);

-- tasks 테이블 생성
CREATE TABLE IF NOT EXISTS stg_prasia_work.tasks (
    task VARCHAR(50) PRIMARY KEY,
    status INT
);
CREATE TABLE IF NOT EXISTS prasia_work.tasks (
    task VARCHAR(50) PRIMARY KEY,
    status INT
);
-- prasia_work 상태값 INSERT
INSERT IGNORE INTO
    stg_prasia_work.tasks (task, status)
VALUES
    ('prasia_work', 0);
INSERT IGNORE INTO
    prasia_work.tasks (task, status)
VALUES
    ('prasia_work', 0);

-- gc_infos 테이블 생성
CREATE TABLE IF NOT EXISTS stg_prasia_work.gc_infos (
    gcid VARCHAR(50) PRIMARY KEY,
    guild_id VARCHAR(50),
    ranking INT,
    world_name VARCHAR(50),
    gc_name VARCHAR(100),
    guild_name VARCHAR(100),
    class VARCHAR(50),
    grade VARCHAR(50),
    gc_level INT,
    update_time INT
);
CREATE TABLE IF NOT EXISTS prasia_work.gc_infos (
    gcid VARCHAR(50) PRIMARY KEY,
    guild_id VARCHAR(50),
    ranking INT,
    world_name VARCHAR(50),
    gc_name VARCHAR(100),
    guild_name VARCHAR(100),
    class VARCHAR(50),
    grade VARCHAR(50),
    gc_level INT,
    update_time INT
);

-- gc_update_history 테이블 생성
CREATE TABLE IF NOT EXISTS stg_prasia_work.gc_update_history (
    id INT AUTO_INCREMENT PRIMARY KEY,
    gcid VARCHAR(50) NOT NULL,
    old_guild_id VARCHAR(50),
    old_ranking INT,
    old_world_name VARCHAR(50),
    old_gc_name VARCHAR(100),
    old_guild_name VARCHAR(100),
    old_class VARCHAR(50),
    old_grade VARCHAR(50),
    old_gc_level INT,
    new_guild_id VARCHAR(50),
    new_ranking INT,
    new_world_name VARCHAR(50),
    new_gc_name VARCHAR(100),
    new_guild_name VARCHAR(100),
    new_class VARCHAR(50),
    new_grade VARCHAR(50),
    new_gc_level INT,
    update_code INT,
    update_time INT NOT NULL,
    description TEXT,
    INDEX idx_gcid_time (gcid, update_time)
);
CREATE TABLE IF NOT EXISTS prasia_work.gc_update_history (
    id INT AUTO_INCREMENT PRIMARY KEY,
    gcid VARCHAR(50) NOT NULL,
    old_guild_id VARCHAR(50),
    old_ranking INT,
    old_world_name VARCHAR(50),
    old_gc_name VARCHAR(100),
    old_guild_name VARCHAR(100),
    old_class VARCHAR(50),
    old_grade VARCHAR(50),
    old_gc_level INT,
    new_guild_id VARCHAR(50),
    new_ranking INT,
    new_world_name VARCHAR(50),
    new_gc_name VARCHAR(100),
    new_guild_name VARCHAR(100),
    new_class VARCHAR(50),
    new_grade VARCHAR(50),
    new_gc_level INT,
    update_code INT,
    update_time INT NOT NULL,
    description TEXT,
    INDEX idx_gcid_time (gcid, update_time)
);

-- gc_update_history 테이블 INSERT 트리거 생성
DELIMITER $$
CREATE TRIGGER stg_prasia_work.before_insert_gc_update_history
    BEFORE INSERT
    ON gc_update_history FOR EACH ROW
BEGIN
    -- update_code: {0: 신규추가, 1: 서버이전, 2: 결사탈퇴, 3: 결사가입, 4: 직업 변경}
    -- update_code 컬럼이 데이터가 없으면 description 추가
    IF NEW.update_code IS NULL THEN
      SET NEW.description = CONCAT("(", FROM_UNIXTIME(NEW.update_time) ,") ", NEW.description);
    -- (2024.05.01 00:22:07) 신규추가/랭킹/서버/닉네임/결사/직업
    ELSEIF NEW.update_code = 0 THEN
      SET NEW.description = CONCAT("(", FROM_UNIXTIME(NEW.update_time) ,") ", "신규추가", "/", NEW.new_ranking, "/", NEW.new_world_name, "/", NEW.new_gc_name, "/", NEW.new_guild_name, "/", NEW.new_class);
    -- (2024.05.01 00:22:07) 서버이전/(old_랭킹 > new_랭킹)/(old_서버 > new_서버)/(old_닉네임 > new_닉네임)/(old_결사 > new_결사)/직업
    ELSEIF NEW.update_code = 1 THEN
      SET NEW.description = CONCAT("(", FROM_UNIXTIME(NEW.update_time) ,") ", "서버이전",
      "(", NEW.old_ranking, " > ", NEW.new_ranking, ")",
      "/", "(", NEW.old_world_name, " > ", NEW.new_world_name, ")",
      "/", "(", NEW.old_gc_name, " > ", NEW.new_gc_name, ")",
      "/", "(", NEW.old_guild_name, " > ", NEW.new_guild_name, ")",
      "/", NEW.new_class);
    -- (2024.05.01 00:22:07) old_결사 결사탈퇴
    ELSEIF NEW.update_code = 2 THEN
      SET NEW.description = CONCAT("(", FROM_UNIXTIME(NEW.update_time) ,") ", NEW.old_guild_name, " ", "결사탈퇴");
    -- (2024.05.01 00:22:07) old_결사 > new_결사 결사가입
    ELSEIF NEW.update_code = 3 THEN
      SET NEW.description = CONCAT("(", FROM_UNIXTIME(NEW.update_time) ,") ", NEW.old_guild_name, " > ", NEW.new_guild_name, " ", "결사가입");
    -- (2024.05.01 00:22:07) old_클래스 > new_클래스 클래스변경
    ELSEIF NEW.update_code = 4 THEN
      SET NEW.description = CONCAT("(", FROM_UNIXTIME(NEW.update_time) ,") ", NEW.old_class, " > ", NEW.new_class, " ", "클래스변경");
    END IF;
END;$$
CREATE TRIGGER prasia_work.before_insert_gc_update_history
    BEFORE INSERT
    ON gc_update_history FOR EACH ROW
BEGIN
    -- update_code: {0: 신규추가, 1: 서버이전, 2: 결사탈퇴, 3: 결사가입, 4: 직업 변경}
    -- update_code 컬럼이 데이터가 없으면 description 추가
    IF NEW.update_code IS NULL THEN
      SET NEW.description = CONCAT("(", FROM_UNIXTIME(NEW.update_time) ,") ", NEW.description);
    -- (2024.05.01 00:22:07) 신규추가/랭킹/서버/닉네임/결사/직업
    ELSEIF NEW.update_code = 0 THEN
      SET NEW.description = CONCAT("(", FROM_UNIXTIME(NEW.update_time) ,") ", "신규추가", "/", NEW.new_ranking, "/", NEW.new_world_name, "/", NEW.new_gc_name, "/", NEW.new_guild_name, "/", NEW.new_class);
    -- (2024.05.01 00:22:07) 서버이전/(old_랭킹 > new_랭킹)/(old_서버 > new_서버)/(old_닉네임 > new_닉네임)/(old_결사 > new_결사)/직업
    ELSEIF NEW.update_code = 1 THEN
      SET NEW.description = CONCAT("(", FROM_UNIXTIME(NEW.update_time) ,") ", "서버이전",
      "(", NEW.old_ranking, " > ", NEW.new_ranking, ")",
      "/", "(", NEW.old_world_name, " > ", NEW.new_world_name, ")",
      "/", "(", NEW.old_gc_name, " > ", NEW.new_gc_name, ")",
      "/", "(", NEW.old_guild_name, " > ", NEW.new_guild_name, ")",
      "/", NEW.new_class);
    -- (2024.05.01 00:22:07) old_결사 결사탈퇴
    ELSEIF NEW.update_code = 2 THEN
      SET NEW.description = CONCAT("(", FROM_UNIXTIME(NEW.update_time) ,") ", NEW.old_guild_name, " ", "결사탈퇴");
    -- (2024.05.01 00:22:07) old_결사 > new_결사 결사가입
    ELSEIF NEW.update_code = 3 THEN
      SET NEW.description = CONCAT("(", FROM_UNIXTIME(NEW.update_time) ,") ", NEW.old_guild_name, " > ", NEW.new_guild_name, " ", "결사가입");
    -- (2024.05.01 00:22:07) old_클래스 > new_클래스 클래스변경
    ELSEIF NEW.update_code = 4 THEN
      SET NEW.description = CONCAT("(", FROM_UNIXTIME(NEW.update_time) ,") ", NEW.old_class, " > ", NEW.new_class, " ", "클래스변경");
    END IF;
END;$$
DELIMITER ;

FLUSH PRIVILEGES;