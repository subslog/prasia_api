# 표준 라이브러리
import sys
from datetime import datetime
# 프로젝트 모듈
from src.utils.json_work import load
from src.utils.db_connector import connect_to_db
from src.utils.db_connector import execute_query
from src.utils.date import convert_time_to_timestamp

def insert_gc_infos(file_path: str) -> None:
    """히스토리 파일의 캐릭터 정보 DB로 INSERT 하는 함수"""
    
    # 히스토리 파일 dict 변환
    gc_history: dict = load(file_path)
    
    # INSERT 데이터 저장 리스트
    insert_data: list = []
    
    for gcid in gc_history['gc_info']:
        # 유닉스 타임스탬프 변환
        unix_time: int = convert_time_to_timestamp(gc_history['gc_info'][gcid]['last_update'])
        
        # INSERT 할 데이터 리스트에 저장
        insert_data.append([gcid, gc_history['gc_info'][gcid]['guild_id'], gc_history['gc_info'][gcid]['ranking'], gc_history['gc_info'][gcid]['world_name'], gc_history['gc_info'][gcid]['gc_name'], gc_history['gc_info'][gcid]['guild_name'], gc_history['gc_info'][gcid]['class'], gc_history['gc_info'][gcid]['grade'], gc_history['gc_info'][gcid]['gc_level'], unix_time])
    
    # DB 연결
    mydb = connect_to_db()
    
    # 데이터 INSERT(gcid 레코드가 있으면 UPDATE)
    qeury: str = """
        INSERT INTO gc_infos (gcid, guild_id, ranking, world_name, gc_name, guild_name, class, grade, gc_level, update_time)
        VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
        ON DUPLICATE KEY UPDATE
        guild_id = VALUES(guild_id),
        ranking = VALUES(ranking),
        world_name = VALUES(world_name),
        gc_name = VALUES(gc_name),
        guild_name = VALUES(guild_name),
        class = VALUES(class),
        grade = VALUES(grade),
        gc_level = VALUES(gc_level),
        update_time = VALUES(update_time);
    """
    execute_query(mydb, qeury, seq_params=insert_data)
    mydb.commit()

def insert_gc_history(file_path: str) -> None:
    """히스토리 파일의 히스토리 정보 DB로 INSERT 하는 함수"""
    
    # 히스토리 파일 dict 변환
    gc_history: dict = load(file_path)
    
    # INSERT 데이터 저장 리스트
    insert_data: list = []
    
    for gcid in gc_history['gc_info']:
        # 캐릭터 히스토리 정보 추가
        for history in gc_history['gc_info'][gcid]['history']:
            # 시간, 설명 분리
            update_time, description = history.split(') ', maxsplit=1)
            # 유닉스 타임스탬프 변환
            unix_time: int = convert_time_to_timestamp(update_time.lstrip('('))
            # INSERT 할 데이터 리스트에 저장
            insert_data.append([gcid, unix_time, description])
    
    # DB 연결
    mydb = connect_to_db()
    
    # 데이터 INSERT
    qeury: str = """
        INSERT IGNORE INTO gc_update_history (gcid, update_time, description)
        VALUES (%s, %s, %s);
    """
    execute_query(mydb, qeury, seq_params=insert_data)
    mydb.commit()
    
if __name__ == '__main__':
    if sys.argv[1] == 'gc_infos':
        insert_gc_infos(sys.argv[2])
    elif sys.argv[1] == 'gc_history':
        insert_gc_history(sys.argv[2])