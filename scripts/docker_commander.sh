#/bin/bash

# 특정 이미지의 컨테이너 ID 조회
get_container_id() {
  # 매개변수가 없으면 예외
  if [ -z $1 ]; then
    echo 'Error: Enter the Docker image name as a parameter' >&2
    exit 1
  fi
  # 특정 이미지의 첫 번째 컨테이너 ID
  echo $(docker ps -q --filter ancestor=$1 | head -n 1)
}

exec_container() {
  # 매개변수가 없으면 예외
  if [ -z $1 ] || [ -z "$2" ]; then
    echo 'Error: Enter Docker container ID and command as parameters' >&2
    exit 1
  fi
  # 컨테이너에 명령어 전달
  docker exec $1 bash -c "$2"
}